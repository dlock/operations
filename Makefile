UNAME=linux-amd64

all: bin/helm

bin:
	mkdir $@

HELM_V=3.0.0-rc.1
HELM_URL=https://get.helm.sh/helm-v$(HELM_V)-$(UNAME).tar.gz

bin/helm-$(HELM_V)-$(UNAME).tar.gz: | bin
	curl $(HELM_URL) > $@

bin/helm-$(HELM_V): bin/helm-$(HELM_V)-$(UNAME).tar.gz
	mkdir -p $@
	tar zxf $< -C $@

bin/helm: bin/helm-$(HELM_V) | bin
	ln -sf $(PWD)/$</$(UNAME)/helm $@
	# Make looks at the mtime of the target
	touch $@

SOPS_V=3.4.0
SOPS_URL=https://github.com/mozilla/sops/releases/download/$(SOPS_V)/sops_$(SOPS_V)_amd64.deb

install-sops: bin/.sops.$(SOPS_V).cookie

bin/.sops.$(SOPS_V).cookie: bin/sops_$(SOPS_V)_amd64.deb
	sudo dpkg -i $<
	touch $@
	
bin/sops_$(SOPS_V)_amd64.deb: | bin
	curl -L $(SOPS_URL) > $@
