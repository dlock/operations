# Generating passwords

    pwgen -nyc 20 1

# Installing

    ./decrypt
    export instance=foo
    helm upgrade --install $instance dlock -f plain.dlock.yaml -f plain.dlock-$instance.yaml
