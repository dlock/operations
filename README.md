# Log

    kubectl create namespace databases
    kubectl apply -f kubernetes/tiller.yaml
    helm init --service-account tiller --history-max 200
    helm install --name dlock-db stable/postgresql --set postgresUser=dlock-admin,postgresDatabase=dlock-db --namespace database

    To get the password for "postgres" run:
    export PGPASSWORD=$(kubectl get secret --namespace database dlock-db-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

    To connect to your database from outside the cluster execute the following commands:
    kubectl port-forward --namespace database dlock-db-postgresql-0 5430:5432 &
    psql --host 127.0.0.1 -U postgres -p 5430 &

# TLS

    gcloud compute addresses create dlock-ingress-address --global
    gcloud compute addresses describe dlock-ingress-address --global

# External DNS

Inspired from: https://tech.paulcz.net/kubernetes-cookbook/gcp/gcp-external-dns/ and https://github.com/kubernetes-incubator/external-dns/blob/master/docs/tutorials/gke.md

    gcloud iam service-accounts create external-dns \
      --display-name "Service account for ExternalDNS on GCP"

    gcloud projects add-iam-policy-binding <GCP_PROJECT_ID> \
      --role='roles/dns.admin' \
      --member='serviceAccount:external-dns@<GCP_PROJECT_ID>.iam.gserviceaccount.com'


    gcloud iam service-accounts keys create credentials.json \
      --iam-account external-dns@<GCP_PROJECT_ID>.iam.gserviceaccount.com

    kubectl -n external-dns-gcp create secret \
      generic external-dns \
      --from-file=credentials.json=credentials.json

